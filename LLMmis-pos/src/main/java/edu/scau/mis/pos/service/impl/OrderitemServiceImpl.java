package edu.scau.mis.pos.service.impl;

import edu.scau.mis.core.utils.DateUtils;
import edu.scau.mis.pos.entity.PosCategory;
import edu.scau.mis.pos.entity.PosItem;
import edu.scau.mis.pos.entity.PosOrder;
import edu.scau.mis.pos.entity.PosOrderitem;
import edu.scau.mis.pos.mapper.CategoryMapper;
import edu.scau.mis.pos.mapper.ItemMapper;
import edu.scau.mis.pos.mapper.OrderMapper;
import edu.scau.mis.pos.mapper.OrderitemMapper;
import edu.scau.mis.pos.service.IOrderitemService;
import edu.scau.mis.pos.vo.ChartVO;
import edu.scau.mis.pos.vo.InputVO;
import edu.scau.mis.pos.vo.OrderVO;
import edu.scau.mis.pos.vo.OrderitemVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (PosOrderitem)表服务实现类
 *
 * @author makejava
 * @since 2021-11-15 16:05:38
 */
@Service("posOrderitemService")
public class OrderitemServiceImpl implements IOrderitemService {
    @Autowired
    private OrderitemMapper orderitemMapper;
    private OrderMapper orderMapper;
    private CategoryMapper categoryMapper;
    private ItemMapper itemMapper;
    @Autowired
    private RedisTemplate redisTemplate;


    //    Map itemQuantityMap = new HashMap<String,Integer>();
//    //判断是否是初次录入商品信息以生成订单号
//    boolean isFirstEnter = true;
//    //根据库存判断是否可以提交订单
//    boolean committable = true;
//    //存储当前的订单编号
//    public static String orderNumber;

    /**
     * @param
     * @return
     * @Description 生成已销售的系列数量报表饼图
     */
    @Override
    public List<ChartVO> countByCategoryId() {
        List<PosCategory> categoryList = this.categoryMapper.queryAll(new PosCategory());
        List<ChartVO> chartVOList = new ArrayList<>();
        for (PosCategory posCategory : categoryList) {
            ChartVO chartVO = new ChartVO();
            int count = this.orderitemMapper.countByCategoryId(posCategory.getCategoryId());
            String name = posCategory.getCategoryName();
            chartVO.setName(name);
            chartVO.setValue(count);
            chartVOList.add(chartVO);
        }
        return chartVOList;
    }


    /**
     * @param
     * @return
     * @Description 创建一个订单的方法
     */
    public InputVO createOrder() {
        String orderNumber = "NO" + DateUtils.dateTimeNow() + String.valueOf(Math.random() * 10000);
        ArrayList<OrderitemVo> orderItemVOList = new ArrayList<>();
        redisTemplate.opsForList().leftPush(orderNumber, orderItemVOList);
        InputVO inputVO = new InputVO();
        inputVO.setOrderNo(orderNumber);
        return inputVO;
    }

    /**
     * @param
     * @return
     * @Description 提交订单操作方法
     * @Author lzc
     * @Date 2021/11/17
     */
    @Override
    public Boolean commitOrder(OrderVO orderVO) {
        boolean committable = true;
        String orderNo = orderVO.getOrderNo();
        //遍历列表里面的orderItemVo，对每一个商品判断库存是否足够，
        List<OrderitemVo> cachelist = redisTemplate.opsForList().range(orderNo, 0, -1);
        for (OrderitemVo orderitemVO : cachelist) {
            PosItem posItem = itemMapper.queryByItemNumber(orderitemVO.getItemNumber());
            System.out.println("debug--------------");
            System.out.println(posItem);
            if (posItem.getInventory() < orderitemVO.getQuantity()) {
                committable = false;
                System.out.println(posItem.getItemName() + "库存不足，返回");
                return false;
            }
        }
        //如果库存不够则设置flag，方法返回操作失败的false
        if (committable) {
            //插入order表生成orderid，将缓存中的OrderVo对象插入到order表中，
            PosOrder posOrder = new PosOrder();
            posOrder.setOrderNo(orderNo);
            posOrder.setDatetime(orderVO.getDatetime());
            posOrder.setTotalamount(orderVO.getTotalAmount());
            posOrder.setTotalquantity(orderVO.getTotalQuantity());
            orderMapper.insert(posOrder);
            System.out.println("debug-----");
            System.out.println("order插入成功");
            Integer orderId = orderMapper.queryOrderByOrderNo(orderNo).getOrderId();
            System.out.println("debug-----");
            System.out.println("成功找到改订单号id" + orderId);
            // 1）则将缓存中的列表数据插入到orderItem表中，更新item表中的库存
            for (OrderitemVo orderitemVO : cachelist) {
                PosItem posItem = itemMapper.queryByItemNumber(orderitemVO.getItemNumber());
                System.out.println("debug-----");
                System.out.println("缓存中的一个posItem为" + posItem.toString());
                PosOrderitem posOrderitem = new PosOrderitem();
                posOrderitem.setOrderId(orderId);
                posOrderitem.setItemId(posItem.getItemId());
                posOrderitem.setSalePrice(orderitemVO.getSalePrice());
                posOrderitem.setQuantity(orderitemVO.getQuantity());
                orderitemMapper.insert(posOrderitem);
                posItem.setInventory(posItem.getInventory() - orderitemVO.getQuantity());
                itemMapper.update(posItem);
            }
            //清空redis缓存中的ordervo和orderitemvo的key，value
            redisTemplate.delete(orderNo);
            //清空前端界面的数据

        }
        return true;
    }


    /**
     * @param
     * @return
     * @Description 查询当前缓存中的订单状态，在输入itemNo同时更新当前订单状态
     * @Author lzc·
     * @Date 2021/11/16
     */
    public OrderVO getCurrentOrderVo(String orderNo) {
        OrderVO orderVO = new OrderVO();
        orderVO.setOrderNo(orderNo);
        orderVO.setDatetime(DateUtils.getNowDate());
        int totalQuantity = 0;
        float totalAmount = 0;
        List<OrderitemVo> cachelist = redisTemplate.opsForList().range(orderNo, 0, -1);
        for (OrderitemVo orderItemVO : cachelist) {
            totalQuantity += orderItemVO.getQuantity();
            totalAmount = totalAmount + orderItemVO.getQuantity() * orderItemVO.getSalePrice();
        }
        orderVO.setTotalAmount(totalAmount);
        orderVO.setTotalQuantity(totalQuantity);
        return orderVO;
    }


    /**
     * @param itemNumber
     * @return
     * @Description 根据商品编号，将商品插入待下单的订单明细集合
     */
    @Override
    public Map<String, Object> enterLineOrderItemVO(InputVO inputVO) {
        //如果机器第一次录入商品，则创建一个订单缓存，生成订单no，放置redis缓存中，并更改判断变量为false
        //根据itemIo查询到item，将item中的编号、名称、价格属性取出来
        //new一个OrderItemVO对象，将编号、名称、价格属性set到Vo中
        String itemNumber = inputVO.getItemNumber();
        String orderNo = inputVO.getOrderNo();
        PosItem posItem = itemMapper.queryByItemNumber(itemNumber);
        //取出订单中的所有商品列表
        List<OrderitemVo> cacheOrderItemVOList = redisTemplate.opsForList().range(orderNo, 0, -1);
        boolean flag = true;
        //如果当前录入的商品已在购物车当中，则在缓存中的该商品数量加一
        for (int i = 0; i < cacheOrderItemVOList.size(); i++) {
            if (cacheOrderItemVOList.get(i).getItemNumber().equals(posItem.getItemNumber())) {
//                cacheOrderItemVOList.get(i).setQuantity(cacheOrderItemVOList.get(i).getQuantity()+1);
                OrderitemVo vo = cacheOrderItemVOList.get(i);
//                redisTemplate.opsForList().remove(orderNo,0,vo);
                vo.setQuantity(vo.getQuantity() + 1);
                redisTemplate.opsForList().set(orderNo, i, vo);
                flag = false;
                break;
            }
        }

        //如果该商品没在购物车中，是一个新商品，则设置数量为1，并将该商品加入缓存中
        if (flag) {
            OrderitemVo orderItemVO = new OrderitemVo();
            orderItemVO.setItemNumber(itemNumber);
            orderItemVO.setItemName(posItem.getItemName());
            orderItemVO.setSalePrice(posItem.getPrice());
            orderItemVO.setQuantity(1);
            redisTemplate.opsForList().rightPush(orderNo, orderItemVO);
        }
        OrderVO currentOrderVo = this.getCurrentOrderVo(orderNo);
        HashMap<String, Object> info = new HashMap<>();
        info.put("orderItemList", redisTemplate.opsForList().range(orderNo, 0, -1));
        info.put("currentOrderVo", currentOrderVo);
        return info;
    }


    /**
     * 通过ID查询单条数据
     *
     * @param orderitemId 主键
     * @return 实例对象
     */
    @Override
    public PosOrderitem queryById(Integer orderitemId) {
        return this.orderitemMapper.queryById(orderitemId);
    }

    /**
     * 查询指定行
     *
     * @param posOrderitem 筛选条件
     * @return 查询结果
     */
    @Override
    public List<PosOrderitem> queryAll(PosOrderitem posOrderitem) {
        return this.orderitemMapper.queryAll(posOrderitem);
    }

    /**
     * 新增数据
     *
     * @param posOrderitem 实例对象
     * @return 实例对象
     */
    @Override
    public PosOrderitem insert(PosOrderitem posOrderitem) {
        this.orderitemMapper.insert(posOrderitem);
        return posOrderitem;
    }

    /**
     * 修改数据
     *
     * @param posOrderitem 实例对象
     * @return 实例对象
     */
    @Override
    public PosOrderitem update(PosOrderitem posOrderitem) {
        this.orderitemMapper.update(posOrderitem);
        return this.queryById(posOrderitem.getOrderitemId());
    }

    /**
     * 通过主键删除数据
     *
     * @param orderitemId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer orderitemId) {
        return this.orderitemMapper.deleteById(orderitemId) > 0;
    }

}


