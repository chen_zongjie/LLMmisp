package edu.scau.mis.pos.vo;

public class InputVO {
    String orderNo;
    String itemNumber;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNo(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    @Override
    public String toString() {
        return "InputVO{" +
                "orderNo='" + orderNo + '\'' +
                ", itemNumber='" + itemNumber + '\'' +
                '}';
    }
}
