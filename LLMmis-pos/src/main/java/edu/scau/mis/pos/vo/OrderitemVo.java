package edu.scau.mis.pos.vo;

import java.io.Serializable;

public class OrderitemVo implements Serializable {
        //名字
        private  String itemName;
        //销售数量
        private Integer quantity;
        //商品编号
        private String itemNumber;
        //销售价格
        private Float salePrice;

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public String getItemNumber() {
            return itemNumber;
        }

        public void setItemNumber(String itemNumber) {
            this.itemNumber = itemNumber;
        }

        public Float getSalePrice() {
            return salePrice;
        }

        public void setSalePrice(Float salePrice) {
            this.salePrice = salePrice;
        }

        @Override
        public String toString() {
            return "OrderItemVO{" +
                    "itemName='" + itemName + '\'' +
                    ", quantity=" + quantity +
                    ", itemNumber='" + itemNumber + '\'' +
                    ", salePrice=" + salePrice +
                    '}';
        }
    }
