package edu.scau.mis.pos.entity;

import java.io.Serializable;

/**
 * (PosCategory)实体类
 *
 */
public class PosCategory implements Serializable {
    private static final long serialVersionUID = -37464733255614026L;
    /**
     * 图书类型ID
     */
    private Integer categoryId;
    /**
     * 类型编号
     */
    private String categoryNo;
    /**
     * 类型名称
     */
    private String categoryName;


    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryNo() {
        return categoryNo;
    }

    public void setCategoryNo(String categoryNo) {
        this.categoryNo = categoryNo;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}