package edu.scau.mis.pos.mapper;

import edu.scau.mis.pos.entity.PosCategory;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * (PosCategory)表数据库访问层
 */
@Mapper
@Repository
public interface CategoryMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param categoryId 主键
     * @return 实例对象
     */
    PosCategory queryById(Integer categoryId);

    /**
     * 查询指定行数据
     *
     * @param posCategory 查询条件
     * @return 对象列表
     */
    List<PosCategory> queryAll(PosCategory posCategory);

    /**P
     * 统计总行数
     *
     * @param posCategory 查询条件
     * @return 总行数
     */
    long count(PosCategory posCategory);

    /**
     * 新增数据
     *
     * @param posCategory 实例对象
     * @return 影响行数
     */
    int insert(PosCategory posCategory);


    /**
     * 修改数据
     *
     * @param posCategory 实例对象
     * @return 影响行数
     */
    int update(PosCategory posCategory);

    /**
     * 通过主键删除数据
     *
     * @param categoryId 主键
     * @return 影响行数
     */
    int deleteById(Integer categoryId);

}