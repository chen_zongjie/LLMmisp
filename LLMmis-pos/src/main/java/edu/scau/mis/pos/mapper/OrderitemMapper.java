package edu.scau.mis.pos.mapper;

import edu.scau.mis.pos.entity.PosOrderitem;
import edu.scau.mis.pos.vo.OrderitemVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * (PosOrderitem)表数据库访问层
 *
 * @author makejava
 * @since 2021-11-15 16:05:30
 */
@Mapper
@Repository
public interface OrderitemMapper {

    /**
     * @Description 根据商品编号将商品添加到订单明细，即页面中的待购买列表
     * @param itemNumber
     * @return
     */
    OrderitemVo queryOrderItemVOByItemNo(String itemNumber);

    /**
     * 通过ID查询单条数据
     *
     * @param orderitemId 主键
     * @return 实例对象
     */
    PosOrderitem queryById(Integer orderitemId);

    /**
     * 查询指定行数据
     *
     * @param posOrderitem 查询条件
     * @return 对象列表
     */
    List<PosOrderitem> queryAll(PosOrderitem posOrderitem);

    /**
     * 统计总行数
     *
     * @param posOrderitem 查询条件
     * @return 总行数
     */
    long count(PosOrderitem posOrderitem);

    /**
     * 新增数据
     *
     * @param posOrderitem 实例对象
     * @return 影响行数
     */
    int insert(PosOrderitem posOrderitem);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<OrderItem> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<PosOrderitem> entities);

    /**
     * 修改数据
     *
     * @param posOrderitem 实例对象
     * @return 影响行数
     */
    int update(PosOrderitem posOrderitem);

    /**
     * 通过主键删除数据
     *
     * @param orderitemId 主键
     * @return 影响行数
     */
    int deleteById(Integer orderitemId);

    /**
     * @Description 根据商品种类id统计销售量，生成报表
     * @param categoryId
     * @return
     */
    int countByCategoryId(Integer categoryId);
}

