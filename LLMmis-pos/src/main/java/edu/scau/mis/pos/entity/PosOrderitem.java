package edu.scau.mis.pos.entity;

import java.io.Serializable;

/**
 * (PosOrderitem)实体类
 *
 * @author makejava
 * @since 2021-11-15 18:31:18
 */
public class PosOrderitem implements Serializable {
    private static final long serialVersionUID = -96464943766216376L;
    
    private Integer orderitemId;
    
    private Float salePrice;
    
    private Integer itemId;
    
    private Integer orderId;
    
    private Integer quantity;



    public Integer getOrderitemId() {
        return orderitemId;
    }

    public void setOrderitemId(Integer orderitemId) {
        this.orderitemId = orderitemId;
    }

    public Float getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Float salePrice) {
        this.salePrice = salePrice;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer orderitemQuantity) {
        this.quantity = quantity;
    }

}

