package edu.scau.mis.pos.service.impl;

import edu.scau.mis.pos.entity.PosItem;
import edu.scau.mis.pos.entity.PosOrder;
import edu.scau.mis.pos.mapper.OrderMapper;
import edu.scau.mis.pos.service.IOrderService;
import edu.scau.mis.pos.vo.OrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * (PosOrder)表服务实现类
 *
 * @author makejava
 * @since 2021-11-13 23:16:18
 */
@Service("posOrderService")
public class OrderServiceImpl implements IOrderService {
    @Resource
    private OrderMapper orderMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public Object redistest() {
        ArrayList<PosItem> list = new ArrayList<>();
        PosItem posItem1 = new PosItem();
        posItem1.setItemName("1");
        PosItem posItem2 = new PosItem();
        posItem2.setItemName("2");
        list.add(posItem2);
        list.add(posItem1);
        redisTemplate.opsForValue().set("ordervo",list);
        return redisTemplate.opsForValue().get("ordervo");
    }


    @Override
    public OrderVO queryOrderByOrderNo(String orderNo) {
        return orderMapper.queryOrderVoByOrderNo(orderNo);
    }

    /**
     * 通过ID查询单条数据
     *
     * @param orderId 主键
     * @return 实例对象
     */
    @Override
    public PosOrder queryById(Integer orderId) {
        return this.orderMapper.queryById(orderId);
    }

    /**
     * 分页查询
     *
     * @param posOrder 筛选条件
     * @return 查询结果
     */
    @Override
    public List<PosOrder> queryAll(PosOrder posOrder) {
        return this.orderMapper.queryAll(posOrder);
    }

    /**
     * 新增数据
     *
     * @param posOrder 实例对象
     * @return 实例对象
     */
    @Override
    public PosOrder insert(PosOrder posOrder) {
        this.orderMapper.insert(posOrder);
        return posOrder;
    }

    /**
     * 修改数据
     *
     * @param posOrder 实例对象
     * @return 实例对象
     */
    @Override
    public PosOrder update(PosOrder posOrder) {
        this.orderMapper.update(posOrder);
        return this.queryById(posOrder.getOrderId());
    }

    /**
     * 通过主键删除数据
     *
     * @param orderId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer orderId) {
        return this.orderMapper.deleteById(orderId) > 0;
    }
}
