package edu.scau.mis.pos.entity;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * (PosOrder)实体类
 *
 * @author makejava
 * @since 2021-11-15 18:31:29
 */
public class PosOrder implements Serializable {
    private static final long serialVersionUID = 796484201158330331L;
    
    private Integer orderId;
    
    private String orderNo;
    
    private Integer totalquantity;
    
    private Float totalamount;
    
    private Date datetime;


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getTotalquantity() {
        return totalquantity;
    }

    public void setTotalquantity(Integer totalquantity) {
        this.totalquantity = totalquantity;
    }

    public Float getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(Float totalamount) {
        this.totalamount = totalamount;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    /**
     * 订单购买的商品集合
     */
    private List<PosItem> posItemList;

    public List<PosItem> getPosItemList() {
        return posItemList;
    }

    public void setPosItemList(List<PosItem> posItemList) {
        this.posItemList = posItemList;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", orderNo='" + orderNo + '\'' +
                ", datetime=" + datetime +
                ", totalAmount=" + totalamount +
                ", totalQuantity=" + totalquantity +
                ", orderItemList=" + posItemList +
                '}';
    }


}

