package edu.scau.mis.pos.service;

import edu.scau.mis.pos.entity.PosCategory;

import java.util.List;

/**
 * (Type)表服务接口
 */
public interface ICategoryService {

    /**
     * 通过ID查询单条数据
     *
     * @param categoryId 主键
     * @return 实例对象
     */
    PosCategory queryById(Integer categoryId);

    /**
     * 查询指定行数据
     *
     * @param posCategory 筛选条件
     * @return 查询结果
     */
    List<PosCategory> queryAll(PosCategory posCategory);

    /**
     * 新增数据
     *
     * @param posCategory 实例对象
     * @return 实例对象
     */
    void insert(PosCategory posCategory);

    /**
     * 修改数据
     *
     * @param posCategory 实例对象
     * @return 实例对象
     */
    void update(PosCategory posCategory);

    /**
     * 通过主键删除数据
     *
     * @param categoryId 主键
     * @return 是否成功
     */
    boolean deleteById(Integer categoryId);

}