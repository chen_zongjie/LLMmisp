package edu.scau.mis.pos.service;

import edu.scau.mis.pos.entity.PosOrderitem;
import edu.scau.mis.pos.vo.ChartVO;
import edu.scau.mis.pos.vo.InputVO;
import edu.scau.mis.pos.vo.OrderVO;

import java.util.List;
import java.util.Map;

/**
 * (PosOrderitem)表服务接口
 *
 * @author makejava
 * @since 2021-11-15 16:05:38
 */
public interface IOrderitemService {

    List<ChartVO> countByCategoryId();

    /**
     * @Description 查询当前缓存中的订单状态，在输入itemNo同时更新当前订单状态
     * @Author lzc·
     * @Date  2021/11/16
     * @param orderNo
     * @return
     */
    OrderVO getCurrentOrderVo(String orderNo);

    InputVO createOrder();

    /**
     * @Description  根据商品编号，将商品插入待下单的订单明细集合
     * @Author lzc
     * @Date  2021/11/15
     * @param inputVO
     * @return
     */
    Map<String, Object> enterLineOrderItemVO(InputVO inputVO);
    /**
     * 通过ID查询单条数据
     *
     * @param orderitemId 主键
     * @return 实例对象
     */
    PosOrderitem queryById(Integer orderitemId);

    /**
     * 查询指定行数据
     *
     * @param posOrderitem 筛选条件
     * @return 查询结果
     */
    List<PosOrderitem> queryAll(PosOrderitem posOrderitem);

    /**
     * 新增数据
     *
     * @param posOrderitem 实例对象
     * @return 实例对象
     */
    PosOrderitem insert(PosOrderitem posOrderitem);

    /**
     * 修改数据
     *
     * @param posOrderitem 实例对象
     * @return 实例对象
     */
    PosOrderitem update(PosOrderitem posOrderitem);

    /**
     * 通过主键删除数据
     *
     * @param orderitemId 主键
     * @return 是否成功
     */
    boolean deleteById(Integer orderitemId);

    Boolean commitOrder(OrderVO orderVO);

}
