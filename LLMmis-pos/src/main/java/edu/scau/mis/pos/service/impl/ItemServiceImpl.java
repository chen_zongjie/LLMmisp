package edu.scau.mis.pos.service.impl;

import edu.scau.mis.pos.entity.PosItem;
import edu.scau.mis.pos.mapper.ItemMapper;
import edu.scau.mis.pos.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * (PosItem)表服务实现类
 */
@Service
public class ItemServiceImpl implements ItemService {
    // @Autowired  // 两种注解效果相同
    @Autowired
    private ItemMapper itemMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param itemId 主键
     * @return 实例对象
     */
    @Override
    public PosItem queryById(Integer itemId) {
        return this.itemMapper.queryById(itemId);
    }

    /**
     * 查所有
     *
     * @param posItem 筛选条件
     * @return 查询结果
     */
    @Override
    public List<PosItem> queryAll(PosItem posItem) {
        return this.itemMapper.queryAll(posItem);
    }

    /**
     * 新增数据
     *
     * @param posItem 实例对象
     * @return 实例对象
     */
    @Override
    public PosItem insert(PosItem posItem) {
        this.itemMapper.insert(posItem);
        return posItem;
    }

    /**
     * 修改数据
     *
     * @param posItem 实例对象
     * @return 实例对象
     */
    @Override
    public PosItem update(PosItem posItem) {
        this.itemMapper.update(posItem);
        return this.queryById(posItem.getItemId());
    }

    /**
     * 通过主键删除数据
     *
     * @param itemId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer itemId) {
        return this.itemMapper.deleteById(itemId) > 0;
    }
}