package edu.scau.mis.pos.service;

import edu.scau.mis.pos.entity.PosItem;

import java.util.List;

/**
 * (PosItem)表服务接口
 *
 * @author makejava
 * @since 2021-10-21 11:23:19
 */
public interface ItemService {

    /**
     * 通过ID查询单条数据
     *
     * @param itemId 主键
     * @return 实例对象
     */
    PosItem queryById(Integer itemId);

    /**
     * 查所有
     *
     * @param posItem 筛选条件
     * @return 查询结果
     */
    List<PosItem> queryAll(PosItem posItem);

    /**
     * 新增数据
     *
     * @param posItem 实例对象
     * @return 实例对象
     */
    PosItem insert(PosItem posItem);

    /**
     * 修改数据
     *
     * @param posItem 实例对象
     * @return 实例对象
     */
    PosItem update(PosItem posItem);

    /**
     * 通过主键删除数据
     *
     * @param itemId 主键
     * @return 是否成功
     */
    boolean deleteById(Integer itemId);

}
