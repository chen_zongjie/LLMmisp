package edu.scau.mis.pos.mapper;

import edu.scau.mis.pos.entity.PosOrder;
import edu.scau.mis.pos.vo.OrderVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * (PosOrder)表数据库访问层
 *
 * @author makejava
 * @since 2021-11-13 23:16:07
 */
@Mapper
@Repository
public interface OrderMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param orderId 主键
     * @return 实例对象
     */
    PosOrder queryById(Integer orderId);

    /**
     * 查询指定行数据
     *
     * @param posOrder 查询条件
     * @return 对象列表
     */
    List<PosOrder> queryAll(PosOrder posOrder);

    /**
     * 统计总行数
     *
     * @param posOrder 查询条件
     * @return 总行数
     */
    long count(PosOrder posOrder);

    /**
     * 新增数据
     *
     * @param posOrder 实例对象
     * @return 影响行数
     */
    int insert(PosOrder posOrder);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Order> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<PosOrder> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Order> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<PosOrder> entities);


    /**
     * 修改数据
     *
     * @param posOrder 实例对象
     * @return 影响行数
     */
    int update(PosOrder posOrder);

    /**
     * 通过主键删除数据
     *
     * @param orderId 主键
     * @return 影响行数
     */
    int deleteById(Integer orderId);

    /**
     * 查询订单含已购买中商品列表
     *
     * @param orderNo 订单编号
     * @return 实例对象
     */
    PosOrder queryOrderWithItemListByOrderNo(String orderNo);

    /**
     * 根据orderNo得到order
     * @param orderNo
     * @return 实例对象
     */
    PosOrder queryOrderByOrderNo(String orderNo);

    OrderVO queryOrderVoByOrderNo(String orderNo);

}

