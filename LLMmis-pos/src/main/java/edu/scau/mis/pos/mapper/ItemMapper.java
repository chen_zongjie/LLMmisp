package edu.scau.mis.pos.mapper;

import edu.scau.mis.pos.entity.PosItem;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * (PosItem)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-21 11:23:13
 */
@Mapper
@Repository
public interface ItemMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param itemId 主键
     * @return 实例对象
     */
    PosItem queryById(Integer itemId);

    /**
     * 查询指定行数据
     *
     * @param posItem 查询条件
     * @return 对象列表
     */
    List<PosItem> queryAll(PosItem posItem);

    /**
     * 统计总行数
     *
     * @param posItem 查询条件
     * @return 总行数
     */
    long count(PosItem posItem);

    /**
     * 新增数据
     *
     * @param posItem 实例对象
     * @return 影响行数
     */
    int insert(PosItem posItem);

    /**
     * 修改数据
     *
     * @param posItem 实例对象
     * @return 影响行数
     */
    int update(PosItem posItem);

    /**
     * 通过主键删除数据
     *
     * @param itemId 主键
     * @return 影响行数
     */
    int deleteById(Integer itemId);

    /**
     * 根据itemNumber查询单个对象
     * @param itemNumber
     * @return 对象实例
     */
    PosItem queryByItemNumber(String itemNumber);
}

