package edu.scau.mis.pos.entity;

import java.io.Serializable;

/**
 * (PosItem)实体类
 *
 * @author makejava
 * @since 2021-10-21 11:23:19
 */
public class PosItem implements Serializable {
    private static final long serialVersionUID = -80242348241818507L;
    
    private Integer itemId;
    
    private String itemNumber;
    
    private String itemName;
    
    private Float price;

    private Integer inventory;
    
    private Integer categoryId;


    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Integer getInventory() {
        return inventory;
    }

    public void setInventory(Integer inventory) {
        this.inventory = inventory;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * 商品类型对象
     * category:item为1:N
     */
    private PosCategory posCategory;

    public PosCategory getCategory() {
        return posCategory;
    }

    public void setCategory(PosCategory posCategory) {
        this.posCategory = posCategory;
    }

    @Override
    public String toString() {
        return "PosItem{" +
                "itemId=" + itemId +
                ", itemNumber='" + itemNumber + '\'' +
                ", itemName='" + itemName + '\'' +
                ", inventory=" + inventory +
                ", categoryId=" + categoryId +
                ", price=" + price +
                ", posCategory=" + posCategory +
                '}';
    }
}

