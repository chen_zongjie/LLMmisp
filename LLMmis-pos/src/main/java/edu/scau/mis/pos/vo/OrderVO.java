package edu.scau.mis.pos.vo;

import java.util.Date;
import java.util.List;

public class OrderVO {

    //订单编号
    private String orderNo;

    /**
     * 销售时间
     */
    private Date datetime;
    /**
     * 总金额
     */
    private Float totalAmount;
    /**
     * 总件数
     */
    private Integer totalQuantity;
    /**
     * 每条商品的销售明细
     */
    private List<OrderitemVo> orderItemVOList;

    @Override
    public String toString() {
        return "OrderVO{" +
                "orderNo='" + orderNo + '\'' +
                ", saleDatetime=" + datetime +
                ", totalAmount=" + totalAmount +
                ", totalQuantity=" + totalQuantity +
                ", orderItemVOList=" + orderItemVOList +
                '}';
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public Float getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public List<OrderitemVo> getOrderitemVOList() {
        return orderItemVOList;
    }

    public void setOrderItemVOList(List<OrderitemVo> orderitemVOList) {
        this.orderItemVOList = orderitemVOList;
    }
}
