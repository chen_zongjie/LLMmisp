package edu.scau.mis.pos.service.impl;

import edu.scau.mis.pos.entity.PosCategory;
import edu.scau.mis.pos.mapper.CategoryMapper;
import edu.scau.mis.pos.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (PosCategory)表服务实现类
 */
@Service
public class CategoryServiceImpl implements ICategoryService {
    @Autowired
    private CategoryMapper categoryMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param categoryId 主键
     * @return 实例对象
     */
    @Override
    public PosCategory queryById(Integer categoryId) {
        return this.categoryMapper.queryById(categoryId);
    }

    /**
     * 查询指定行数据
     *
     * @param posCategory 筛选条件
     * @return 查询结果
     */
    @Override
    public List<PosCategory> queryAll(PosCategory posCategory) {
        return this.categoryMapper.queryAll(posCategory);
    }

    /**
     * 新增数据
     * @param posCategory 实例对象
     */
    @Override
    public void insert(PosCategory posCategory) {
        this.categoryMapper.insert(posCategory);
    }

    /**
     * 修改数据
     *
     * @param posCategory 实例对象
     */
    @Override
    public void update(PosCategory posCategory) {
        this.categoryMapper.update(posCategory);
    }

    /**
     * 通过主键删除数据
     *
     * @param categoryId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer categoryId) {
        return this.categoryMapper.deleteById(categoryId) > 0;
    }
}