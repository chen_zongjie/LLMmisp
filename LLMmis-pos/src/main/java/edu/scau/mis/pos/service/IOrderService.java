package edu.scau.mis.pos.service;

import edu.scau.mis.pos.entity.PosOrder;
import edu.scau.mis.pos.vo.OrderVO;

import java.util.List;

/**
 * (PosOrder)表服务接口
 *
 * @author makejava
 * @since 2021-11-13 23:16:17
 */
public interface IOrderService {

    /**
     * 通过ID查询单条数据
     *
     * @param orderId 主键
     * @return 实例对象
     */
    PosOrder queryById(Integer orderId);

    /**
     * 分页查询
     *
     * @param posOrder 筛选条件
     * @return 查询结果
     */
    List<PosOrder> queryAll(PosOrder posOrder);

    /**
     * 新增数据
     *
     * @param posOrder 实例对象
     * @return 实例对象
     */
    PosOrder insert(PosOrder posOrder);

    /**
     * 修改数据
     *
     * @param posOrder 实例对象
     * @return 实例对象
     */
    PosOrder update(PosOrder posOrder);

    /**
     * 通过主键删除数据
     *
     * @param orderId 主键
     * @return 是否成功
     */
    boolean deleteById(Integer orderId);

    Object redistest();

    OrderVO queryOrderByOrderNo(String orderNo);
}
