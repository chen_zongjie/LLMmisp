package edu.scau.mis.test.pos.mapper;

import edu.scau.mis.pos.entity.PosItem;
import edu.scau.mis.pos.mapper.ItemMapper;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
//@SpringBootTest //注意不使用@SpringBootTest
@MybatisTest    //启动MybatisTest
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)    //这个是启用自己配置的数据源

public class ItemMapperTest {
    @Autowired
    private ItemMapper itemMapper;

    @org.junit.Test
    public void queryById() {
        PosItem posItem = itemMapper.queryById(1);
        System.out.println(posItem.getItemName());
    }

    @org.junit.Test
    @Rollback(false) //这个是默认是回滚，不会commit入数据库，改成false 则commit
    public void queryAll() {
        List<PosItem> list =  itemMapper.queryAll(new PosItem());
        for(PosItem type:list){
            System.out.println(type.getItemName());
        }
    }

    @org.junit.Test
    public void count() {
        Assert.assertEquals(6,itemMapper.count(new PosItem())); //本机当前数据库记录数为7
    }

    @org.junit.Test
    public void insert() {
        PosItem posItem = new PosItem();
        posItem.setItemNumber("11");
        posItem.setItemName("测试");
        posItem.setPrice(1.0f);
        posItem.setInventory(1);
        itemMapper.insert(posItem);
    }

    @org.junit.Test
    public void update() {
        PosItem posItem = itemMapper.queryById(1);
        posItem.setItemName("雪碧");  //原名称 可口可乐
        itemMapper.update(posItem);
    }

    @org.junit.Test
    public void deleteById() {
        itemMapper.deleteById(8);  //insert测试添加的记录id为7
    }
}