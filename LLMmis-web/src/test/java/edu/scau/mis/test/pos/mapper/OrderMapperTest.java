package edu.scau.mis.test.pos.mapper;

import edu.scau.mis.pos.entity.PosItem;
import edu.scau.mis.pos.entity.PosOrder;
import edu.scau.mis.pos.entity.PosOrderitem;
import edu.scau.mis.pos.mapper.OrderMapper;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;
@MybatisTest
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class OrderMapperTest {
    @Autowired
    private OrderMapper orderMapper;

    @org.junit.Test
    public void queryById() {
        PosOrder posOrder = orderMapper.queryById(1);//orderNo为A001
        List<PosItem> posItems = posOrder.getPosItemList();
        System.out.println("订单编号为"+posOrder.getOrderNo()
                + ",商品总金额为" + posOrder.getTotalamount()
                + ",商品总件数为"+posItems.size()+"：");
        for(PosItem item : posItems){
            System.out.println("商品ID为"+item.getItemId() + ", 商品为" + item.getItemName());
        }
    }

    @org.junit.Test
    public void queryAll() {
    }

    @org.junit.Test
    public void count() {
    }

    @org.junit.Test
    public void insert() {
    }

    @org.junit.Test
    public void update() {
    }

    @org.junit.Test
    public void deleteById() {
    }
}