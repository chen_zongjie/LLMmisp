package edu.scau.mis.test.pos.mapper;

import edu.scau.mis.pos.entity.PosOrderitem;
import edu.scau.mis.pos.mapper.OrderitemMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
//@SpringBootTest //注意不使用@SpringBootTest
@MybatisTest    //启动MybatisTest
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)    //这个是启用自己配置的数据源

public class OrderItemMapperTest {
    @Autowired
    private OrderitemMapper orderitemMapper;

    @org.junit.Test
    public void queryById() {
        PosOrderitem posOrderitem = orderitemMapper.queryById(1);
        System.out.println(posOrderitem.getOrderId());
    }

    @org.junit.Test
    @Rollback(false) //这个是默认是回滚，不会commit入数据库，改成false 则commit
    public void queryAll() {
        List<PosOrderitem> list =  orderitemMapper.queryAll(new PosOrderitem());
        for(PosOrderitem type:list){
            System.out.println(type.getOrderId());
        }
    }

    @org.junit.Test
    public void count() {
        Assert.assertEquals(1,orderitemMapper.count(new PosOrderitem())); //本机当前数据库记录数为2
    }

    @Test
    public void insert() {
        PosOrderitem posOrderitem = new PosOrderitem();
        posOrderitem.setOrderId(1);
        posOrderitem.setSalePrice(1F);
        posOrderitem.setQuantity(1);
        posOrderitem.setItemId(1);
        orderitemMapper.insert(posOrderitem);
    }

    @org.junit.Test
    public void update() {
        PosOrderitem posOrderitem = orderitemMapper.queryById(1);
        posOrderitem.setOrderId(1);  //原名称 可口可乐
        orderitemMapper.update(posOrderitem);
    }

    @org.junit.Test
    public void deleteById() {
        orderitemMapper.deleteById(8);  //insert测试添加的记录id为7
    }
}