package edu.scau.mis.test.pos.mapper;

import edu.scau.mis.pos.entity.PosCategory;
import edu.scau.mis.pos.mapper.CategoryMapper;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;
//@SpringBootTest //注意不使用@SpringBootTest
@MybatisTest    //启动MybatisTest
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)    //这个是启用自己配置的数据源

public class CategoryMapperTest {
    @Autowired
    private CategoryMapper categoryMapper;

    @org.junit.Test
    public void queryById() {
        PosCategory posCategory = categoryMapper.queryById(1);
        System.out.println(posCategory.getCategoryName());
    }

    @org.junit.Test
    @Rollback(false) //这个是默认是回滚，不会commit入数据库，改成false 则commit
    public void queryAll() {
        List<PosCategory> list =  categoryMapper.queryAll(new PosCategory());
        for(PosCategory category:list){
            System.out.println(category.getCategoryName());
        }
    }

    @org.junit.Test
    public void count() {
        Assert.assertEquals(6,categoryMapper.count(new PosCategory())); //本机当前数据库记录数为6
    }

    @org.junit.Test
    public void insert() {
        PosCategory posCategory = new PosCategory();
        posCategory.setCategoryNo("C999");
        posCategory.setCategoryName("测试");
        categoryMapper.insert(posCategory);
    }

    @org.junit.Test
    public void update() {
        PosCategory posCategory = categoryMapper.queryById(1);
        posCategory.setCategoryName("现代软件工程");  //原名称 软件工程
        categoryMapper.update(posCategory);
    }

    @org.junit.Test
    public void deleteById() {
        categoryMapper.deleteById(7);  //insert测试添加的记录id为7
    }
}