package edu.scau.mis.pos.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import edu.scau.mis.pos.entity.PosOrder;
import edu.scau.mis.pos.service.IOrderService;
import edu.scau.mis.pos.vo.OrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * (Order)表控制层
 *
 * @author CJW
 * @since 2021-11-15 15:14:34
 */


@RestController
@RequestMapping("order")
public class OrderController {
    /**
     * 服务对象
     */
    @Autowired
    private IOrderService orderService;




    @GetMapping("/redis")
    public Object queryById() {
        Object redistest = orderService.redistest();
        return redistest;
    }

    /**
     * 通过订单编号No查询单个订单
     *
     * @param orderNo 主键
     * @return 单条数据
     */
    @GetMapping("/queryOrder/{orderNo}")
    public ResponseEntity<OrderVO> queryOrderByOrderNo(@PathVariable("orderNo") String orderNo) {
        return ResponseEntity.ok(this.orderService.queryOrderByOrderNo(orderNo));
    }

    /**
     * 分页查询
     *
     * @param posOrder 筛选条件
     * @return 查询结果
     */
    @GetMapping("/list")
    public ResponseEntity<List<PosOrder>> queryAll(PosOrder posOrder) {
        return ResponseEntity.ok(this.orderService.queryAll(posOrder));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param orderId 主键
     * @return 单条数据
     */
    @GetMapping("{orderId}")
    public ResponseEntity<PosOrder> queryById(@PathVariable("orderId") Integer orderId) {
        return ResponseEntity.ok(this.orderService.queryById(orderId));
    }

    /**
     * 新增数据
     *
     * @param posOrder 实体
     * @return 新增结果
     */
    @PostMapping
    public ResponseEntity<PosOrder> add(@RequestBody PosOrder posOrder) {
        return ResponseEntity.ok(this.orderService.insert(posOrder));
    }

    /**
     * 编辑数据
     *
     * @param posOrder 实体
     * @return 编辑结果
     */
    @PutMapping
    public ResponseEntity<PosOrder> edit(@RequestBody PosOrder posOrder) {
        return ResponseEntity.ok(this.orderService.update(posOrder));
    }

    /**
     * 删除数据
     *
     * @param orderId 主键
     * @return 删除是否成功
     */
    @DeleteMapping("/delete/{orderId}")
    public ResponseEntity<Boolean> deleteById(@PathVariable Integer orderId) {
        return ResponseEntity.ok(this.orderService.deleteById(orderId));
    }

    @GetMapping("/page")
    public PageInfo page(@RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize, PosOrder posOrder) {
        PageHelper.startPage(pageNum,pageSize);
        List<PosOrder> list = this.orderService.queryAll(posOrder);
        PageInfo<List> pageInfo = new PageInfo(list);
        return pageInfo;
    }
}

