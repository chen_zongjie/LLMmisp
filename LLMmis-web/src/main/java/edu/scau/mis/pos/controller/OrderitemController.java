package edu.scau.mis.pos.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import edu.scau.mis.pos.entity.PosOrderitem;
import edu.scau.mis.pos.mapper.ItemMapper;
import edu.scau.mis.pos.service.IOrderitemService;
import edu.scau.mis.pos.vo.ChartVO;
import edu.scau.mis.pos.vo.InputVO;
import edu.scau.mis.pos.vo.OrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * (OrderItem)表控制层
 *
 * @author CJW
 * @since 2021-11-15 15:14:47
 */
@RestController
@RequestMapping("orderItem")
public class OrderitemController {
    /**
     * 服务对象
     */
    @Autowired
    private IOrderitemService orderitemService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private ItemMapper itemMapper;

    @GetMapping("/createOrder")
    public ResponseEntity<InputVO> createOrder(){
        return ResponseEntity.ok(this.orderitemService.createOrder());
    }

    @GetMapping("/commitOrder")
    public Boolean commitOrder(OrderVO orderVO){
        return this.orderitemService.commitOrder(orderVO);
    }

    @GetMapping("/pie")
    public ResponseEntity<List<ChartVO>> pie()
    {
        return ResponseEntity.ok(this.orderitemService.countByCategoryId());
    }
    @GetMapping("/identifyItem")
    public ResponseEntity<Map<String,Object>> identifyItem(InputVO inputVO) {
        return ResponseEntity.ok(this.orderitemService.enterLineOrderItemVO(inputVO));
    }


    /**
     * 分页查询
     *
     * @param posOrderitem 筛选条件
     * @return 查询结果
     */
    @GetMapping("/list")
    public ResponseEntity<List<PosOrderitem>> queryAll(PosOrderitem posOrderitem) {
        return ResponseEntity.ok(this.orderitemService.queryAll(posOrderitem));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param orderitemId 主键
     * @return 单条数据
     */
    @GetMapping("{orderitemId}")
    public ResponseEntity<PosOrderitem> queryById(@PathVariable("orderitemId") Integer orderitemId) {
        return ResponseEntity.ok(this.orderitemService.queryById(orderitemId));
    }

    /**
     * 编辑数据
     *
     * @param posOrderitem 实体
     * @return 编辑结果
     */
    @PutMapping
    public ResponseEntity<PosOrderitem> edit(@RequestBody PosOrderitem posOrderitem) {
        return ResponseEntity.ok(this.orderitemService.update(posOrderitem));
    }

    /**
     * 删除数据
     *
     * @param orderitemId 主键
     * @return 删除是否成功
     */
    @DeleteMapping("/delete/{orderitemId}")
    public ResponseEntity<Boolean> deleteById(@PathVariable Integer orderitemId) {
        return ResponseEntity.ok(this.orderitemService.deleteById(orderitemId));
    }

    @GetMapping("/page")
    public PageInfo page(@RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize, PosOrderitem posOrderitem) {
        PageHelper.startPage(pageNum,pageSize);
        List<PosOrderitem> list = this.orderitemService.queryAll(posOrderitem);
        PageInfo<List> pageInfo = new PageInfo(list);
        return pageInfo;
    }
}

