package edu.scau.mis.pos.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import edu.scau.mis.pos.entity.PosItem;
import edu.scau.mis.pos.service.ItemService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (PosItem)表控制层
 *
 */
@RestController
@RequestMapping("item")
public class ItemController {
    /**
     * 服务对象
     */
    @Resource
    private ItemService itemService;


    /**
     * 查所有
     *
     * @param posItem 筛选条件
     * @return 查询结果
     */
    @GetMapping("/list")
    public List<PosItem> list(PosItem posItem) {
        return this.itemService.queryAll(posItem);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param itemId 主键
     * @return 单条数据
     */
    @GetMapping("{itemId}")
    public PosItem queryById(@PathVariable("itemId") Integer itemId) {
        return this.itemService.queryById(itemId);
    }

    /**
     * 新增数据
     *
     * @param posItem 实体
     * @return 新增结果
     */
    @PostMapping
    public String add(PosItem posItem) {
        this.itemService.insert(posItem);
        return "ok";
    }

    /**
     * 编辑数据
     *
     * @param posItem 实体
     * @return 编辑结果
     */
    @PutMapping
    public String edit(PosItem posItem) {
        this.itemService.update(posItem);
        return "ok";
    }

    /**
     * 删除数据
     *
     * @param itemId 主键
     * @return 删除是否成功
     */
    @DeleteMapping("/delete/{itemId}")
    public boolean deleteById(@PathVariable Integer itemId) {
        return this.itemService.deleteById(itemId);
    }

    /**
     * 分页查询
     * @param pageNum 起始页
     * @param pageSize 每页数据大小
     * @param posItem 筛选条件
     * @return 查询结果
     */
    @GetMapping("/page")
    public PageInfo page(@RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize, PosItem posItem) {
        PageHelper.startPage(pageNum,pageSize);
        List<PosItem> list = itemService.queryAll(posItem);
        PageInfo<List> pageInfo = new PageInfo(list);
        return pageInfo;
    }


}