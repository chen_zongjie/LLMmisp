package edu.scau.mis.pos.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pos")
public class IndexController {
    @GetMapping("/index")
    public String index(){
        return "Hello SpringBoot";
    }

}