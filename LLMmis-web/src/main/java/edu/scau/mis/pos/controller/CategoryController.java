package edu.scau.mis.pos.controller;


import edu.scau.mis.pos.entity.PosCategory;
import edu.scau.mis.pos.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * (PosCategory)表控制层
 */
@RestController
@RequestMapping("category")
public class CategoryController {
    /**
     * 服务对象
     */
    @Autowired
    private ICategoryService categoryService;

    /**
     * 分页查询
     *
     * @param posCategory 筛选条件
     * @return 查询结果
     */
    @GetMapping("/list")
    public List<PosCategory> list(PosCategory posCategory) {
        return this.categoryService.queryAll(posCategory);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param categoryId 主键
     * @return 单条数据
     */
    @GetMapping("{categoryId}")
    public PosCategory getById(@PathVariable("categoryId") Integer categoryId) {
        return this.categoryService.queryById(categoryId);
    }

    /**
     * 新增数据
     *
     * @param posCategory 实体
     * @return 新增结果
     */
    @PostMapping
    public String add(@RequestBody PosCategory posCategory) {
        this.categoryService.insert(posCategory);
        return "ok";
    }

    /**
     * 编辑数据
     * @param posCategory 实体
     * @return 编辑结果
     */
    @PutMapping
    public String edit(@RequestBody PosCategory posCategory) {
        this.categoryService.update(posCategory);
        return "ok";
    }

    /**
     * 删除数据
     *
     * @param categoryId 主键
     * @return 删除是否成功
     */
    @DeleteMapping("/delete/{categoryId}")
    public Boolean deleteById(@PathVariable Integer categoryId) {
        return this.categoryService.deleteById(categoryId);
    }

}